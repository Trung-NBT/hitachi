import React from "react";
// import logo from "./logo.svg";
import disconnect from "./disconnect.jpg";
// import './App.css';
class Hitachi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      els: []
    };
  }
  componentDidMount() {
    fetch("http://222.252.27.8:9090/cameras")
      .then(res => res.json())
      .then(
        result => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  Capture(elem, id) {
    var canvas = document.createElement("canvas");
    var img = elem.target.parentNode.previousElementSibling.firstChild;
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;
    var today = new Date();
    var M = today.getMonth() + 1;
    var D = today.getDate();
    if (D < 10) {
      D = "0" + D.toString();
    }
    if (M < 10) {
      M = "0" + M.toString();
    }
    var date = D + "/" + M + "/" + today.getFullYear();
    var time =
      today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
    var dateTime = date + " " + time.toString();
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    var uri = canvas
      .toDataURL("image/jpeg")
      .replace("image/jpeg", "image/octet-stream");
    var downloadLink = document.createElement("a");
    downloadLink.href = uri;
    downloadLink.download = "snapshot " + id + " " + dateTime.toString() + " GMT.jpeg";

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }
  render() {
    const dta = [];
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      items.map((item: any, key: any) => {
        if (item.connected === false) {
          if (key % 2 == 0) {
            dta.push(
              <div className="col-md-6 video-camera1 pb-3" key={key}>
                <div className="title-camera">
                  <h2>{item.id}</h2>
                </div>
                <div>
                  <img src={disconnect} alt="CAMERA" className="w-100"></img>
                </div>
                <div className="text-md-right view-cap">
                  <a href={item.log} target="_blank">
                    <button className="btn-view">View Log</button>
                  </a>
                  <button onClick={data => this.Capture(data, item.id)}>
                    Capture
                  </button>
                </div>
              </div>
            );
          } else {
            dta.push(
              <div className="col-md-6 video-camera2 pb-3" key={key}>
                <div className="title-camera">
                  <h2>{item.id}</h2>
                </div>
                <div>
                  <img
                    src={disconnect}
                    alt="CAMERA"
                    className="w-100"
                    crossOrigin="anonymous"
                  />
                </div>
                <div className="text-md-right view-cap">
                  <a href={item.log} target="_blank">
                    <button className="btn-view">View Log</button>
                  </a>
                  <button onClick={data => this.Capture(data, item.id)}>
                    Capture
                  </button>
                </div>
              </div>
            );
          }
        }
        if (item.connected === true) {
          if (key % 2 === 0) {
            dta.push(
              <div className="col-md-6 video-camera1 pb-3" key={key}>
                <div className="title-camera">
                  <h2>{item.id}</h2>
                </div>
                <div>
                  <img
                    src={item.stream}
                    alt="CAMERA"
                    className="w-100"
                    crossOrigin="anonymous"
                  />
                </div>
                <div className="text-md-right view-cap">
                  <a href={item.log} target="_blank">
                    <button className="btn-view">View Log</button>
                  </a>
                  <button onClick={data => this.Capture(data, item.id)}>
                    Capture
                  </button>
                </div>
              </div>
            );
          } else {
            dta.push(
              <div className="col-md-6 video-camera2 pb-3" key={key}>
                <div className="title-camera">
                  <h2>{item.id}</h2>
                </div>
                <div>
                  <img
                    src={item.stream}
                    alt="CAMERA"
                    className="w-100"
                    crossOrigin="anonymous"
                  />
                </div>
                <div className="text-md-right view-cap">
                  <a href={item.log} target="_blank">
                    <button className="btn-view">View Log</button>
                  </a>
                  <button onClick={data => this.Capture(data, item.id)}>
                    Capture
                  </button>
                </div>
              </div>
            );
          }
        }
      });
      return dta;
    }
  }
}
export default Hitachi;
